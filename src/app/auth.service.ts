import { Injectable } from '@angular/core';

declare var Keycloak: any;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  static auth: any = {};

  static init(): Promise<any> {
    let keycloakAuth: any = new Keycloak({
      url: "https://gmk.alexandra.dk/auth",
      realm: 'KomTel', // todo
      clientId: 'thomas' // todo
    });

    AuthService.auth.loggedIn = false;

    return new Promise((resolve, reject) => {
      keycloakAuth.init({ onLoad: 'login-required', responseMode: 'query', checkLoginIframe: false })
      .success(() => {
        AuthService.auth.loggedIn = true;
        AuthService.auth.authz = keycloakAuth;
        console.log("login successful")
        resolve();
      })
      .error(() => {
        console.log("login failed")
        reject();
      });
    });
  }

  logout() {
    console.log('*** LOGOUT');
    AuthService.auth.loggedIn = false;
    if(AuthService.auth.authz) {
      AuthService.auth.authz.logout()
      AuthService.auth.authz = null;
    }


    AuthService.init();
    //window.location.href = AuthService.auth.logoutUrl;
  }

  getToken(): Promise<string> {
    return new Promise<string>((resolve, reject) => {

      if (AuthService && AuthService.auth && AuthService.auth.authz && AuthService.auth.authz.token) {
        AuthService.auth.authz.updateToken(5)
        .success(() => {
          resolve(<string>AuthService.auth.authz.token);
        })
        .error(() => {
          reject('Failed to refresh token');
        });
      } else {
        reject('Token no longer valid')
      }
    });
  }
}
