import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { QuestionnairePage } from './questionnaire.page';
import { QuestionnaireItemComponent } from '../components/questionnaire-item/questionnaire-item.component';

const routes: Routes = [
  {
    path: '',
    component: QuestionnairePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [QuestionnairePage, QuestionnaireItemComponent]
})
export class QuestionnairePageModule {}
