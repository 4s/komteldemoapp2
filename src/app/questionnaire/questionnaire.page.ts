import { Component, OnInit } from '@angular/core';
import QuestionnaireEngine, { Questionnaire } from "questionnaire-engine";
import { FhirClient } from 'ng-fhir/FhirClient';

import { AuthService } from '../auth.service';
import { EngineService } from "../engine.service";

import * as fhir from 'fhir';
import PackagingEngine from "packaging-engine";

@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.page.html',
  styleUrls: ['./questionnaire.page.scss'],
})
export class QuestionnairePage implements OnInit {

  public questionnaire: Questionnaire;
  public title: string;
  private questionnaireEngine: QuestionnaireEngine;
  private packagingEngine: PackagingEngine;

  constructor(
    private auth: AuthService,
    public engine: EngineService
  ) {
    this.packagingEngine = this.engine.getPackagingEngine();
    this.questionnaireEngine = this.engine.getQuestionnaireEngine();
  }

  ionViewDidLoad() {
  }

  setAnswer(event) {
    this.engine.getQuestionnaireEngine().setAnswer(event.linkId, event.answer);
  }

  submitQuestionnaireResponse() {
    this.questionnaireEngine.getQuestionnaireResponse().questionnaire = 'http://fhir.example.com/Questionnaire/1'
    this.questionnaireEngine.getQuestionnaireResponse().basedOn = [{
      identifier: {
        "system": "urn:ietf:rfc:3986",
        "value": "d9636b26-63b5-48ef-961e-8539c086d111"
      },
      type: 'CarePlan'
    }]
    this.questionnaireEngine.getQuestionnaireResponse().status = "completed";
    this.questionnaireEngine.getQuestionnaireResponse().author = {
      identifier: {
        system: "urn:ietf:rfc:3986",
        value: "46168eeb-c75c-4f32-bb7b-da21cb1b446a"
      }
    }
    let questionnaireResponse: fhir.QuestionnaireResponse = this.questionnaireEngine.serializeQuestionnaireResponse();

    const patient = this.packagingEngine
      .getPatientBuilder()
      .addName(
        this.packagingEngine
          .getHumanNameBuilder()
          .addGivenName("John")
          .setFamilyName("Doe")
          .setUse("usual")
          .build()
      )
      .setActive(true)
      .addIdentifier(
        this.packagingEngine
          .getIdentifierBuilder()
          .setSystem("system-for-brugsaftale")
          .setValue("12-15-16-17-17")
          .build()
      )
      .build();
    this.packagingEngine.addRequestEntry(
      this.packagingEngine
        .getEntryBuilder()
        .setMethod("POST")
        .setRequestCondition(
          "ifNoneExist",
          "identifier=" +
          patient.identifier[0].system +
          "|" +
          patient.identifier[0].value
        )
        .setResource(patient)
        .build()
    );
    this.packagingEngine.addRequestEntry(
      this.packagingEngine
        .getEntryBuilder()
        .setMethod("POST")
        .setRequestCondition(
          "ifNoneExist",
          "identifier=" +
          questionnaireResponse.identifier.system +
          "|" +
          questionnaireResponse.identifier.value
        )
        .setResource(questionnaireResponse)
        .build()
    );

    console.log(JSON.stringify(this.packagingEngine.package()))

    let token = '';

    var config = {
      'baseUrl': 'https://telemed-test.rm.dk/gmk-fhir-input-service/baseR4/',
      'credentials': 'same-origin',
      'auth': {
        'bearer': token,
      },
      'headers': {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    };

    let smart = new FhirClient(config);

    let ctgBundle = {
      type: 'Bundle',
      data: JSON.stringify(this.packagingEngine.package()),
      credentials: 'same-origin',
      auth: {
        bearer: token
      }
    }

    smart.create(ctgBundle).then(e => {
      console.log(e);
    }).catch(error => {});
  }

  ngOnInit() {
  }
}
