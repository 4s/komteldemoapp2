import { Injectable } from '@angular/core';
import QuestionnaireEngine, {
  FhirParser,
  FhirSerializer
} from "questionnaire-engine";
import DeviceMeasurementEngine, {
  PHGCoreDeviceManager
} from "device-measurement-engine";
import PackagingEngine from "packaging-engine";
import { baseplate } from 'phg-js-baseplate';


@Injectable({
  providedIn: 'root'
})
export class EngineService {

  constructor() {}

  private questionnaire: QuestionnaireEngine = new QuestionnaireEngine(
    new FhirParser(),
    new FhirSerializer()
  );
  private deviceMeasurement: DeviceMeasurementEngine;
  private packaging: PackagingEngine = new PackagingEngine();

  public getQuestionnaireEngine(): QuestionnaireEngine {
    if (this.questionnaire == null) {
      this.questionnaire = new QuestionnaireEngine(
        new FhirParser(),
        new FhirSerializer()
      );
    }
    return this.questionnaire;
  }

  public getDeviceMeasurementEngine(): DeviceMeasurementEngine {
    if (this.deviceMeasurement == null) {
      this.deviceMeasurement = new DeviceMeasurementEngine(
        new PHGCoreDeviceManager(baseplate)
      );
    }
    return this.deviceMeasurement;
  }

  public getPackagingEngine(): PackagingEngine {
    if (this.packaging == null) {
      this.packaging = new PackagingEngine();
    }
    return this.packaging;
  }
}