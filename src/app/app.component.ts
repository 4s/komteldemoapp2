import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Callback } from './callback'
import * as wrapper from 'phg-js-baseplate'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  private baseplateCallback: Callback

  constructor(private platform: Platform, private splashScreen: SplashScreen, private statusBar: StatusBar ) {
    this.baseplateCallback = {
      error(error) {
        console.log(error);
      },
      success(message) {
        console.log(message);
      }
    }
    this.initializeApp();
  }

  private initializeApp() {
    this.platform.ready().then(() => {
      // Following line can be uncommented to perform a fake initialization of the baseplate
      //this.fakeBaseplateInitialization()
      
      this.initializeBaseplate();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  /**
   * Initializes the baseplate using a function defined in the cordova-adaptor project.
   * 
   * If this is executed in a context without cordova an error is logged and the method returns.
   */
  private initializeBaseplate() {
    var piping
    try {
      piping = cordova.require("dk.s4.phg.cordova-adaptor.BaseplatePiping")
    } catch (e) {
      console.log("Cordova require of piping failed, baseplate is unitialized: " + e)
      return
    }
    piping(wrapper, this.baseplateCallback);
  }
  
  private fakeBaseplateInitialization() {
    wrapper.initialize(function(a) {})
    console.log("### Init: " + wrapper.baseplate.isAvailable())
  }
}
