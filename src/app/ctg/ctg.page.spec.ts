import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CTGPage } from './ctg.page';

describe('CTGPage', () => {
  let component: CTGPage;
  let fixture: ComponentFixture<CTGPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CTGPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CTGPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
