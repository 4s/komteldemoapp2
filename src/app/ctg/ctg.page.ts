import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from '../auth.service';
import { MonicaDeviceConnector, PHGCoreDeviceManager } from 'device-measurement-engine';
import fhir, { BundleEntry } from 'fhir'
import PackagingEngine from 'packaging-engine'
import { baseplate } from 'phg-js-baseplate';

@Component({
  selector: 'app-ctg',
  templateUrl: './ctg.page.html',
  styleUrls: ['./ctg.page.scss'],
})
export class CTGPage implements OnInit {
  log: string[] = [];
  deviceConnector: MonicaDeviceConnector;

  status = '';

  constructor(private auth: AuthService, private zone: NgZone) { }

  ngOnInit() { }

  public start() {
    if(!baseplate.isAvailable()) {
      this.log.unshift('Baseplate not initialized, unable to perform CTG');
      return
    }

    new PHGCoreDeviceManager(baseplate).searchForDevices(['585728']).then(devices => {
      this.deviceConnector = <MonicaDeviceConnector> devices[0]
      if(typeof this.deviceConnector === 'undefined') {
        this.log.unshift('No CTG capable devices found');
        return
      }
      this.deviceConnector.subscribeToStatus((statusMessage: string) => {
        this.zone.run(() => {
          this.status = statusMessage;
        });
      });
      this.deviceConnector.startMeasurement()
        .then(success => {
          this.log.unshift('Starting... ' + success);
        });
    })
  }

  public stop() {
    this.deviceConnector.stopMeasurement()
      .then(success => {
        this.log.unshift("Stopping..." + success);
        return this.deviceConnector.getDeviceAndMeasurement()
      }).then(result => {
        this.log.unshift(JSON.stringify(result));
      }).catch(error => {
        this.log.unshift("error " + error);
      });
  }

  public send() {
     this.deviceConnector.getDeviceAndMeasurement().then(result => {
      const device = result.device;
      // todo construct this url
      device.url = 'Device/008098FEFF0E3913.0080980E3913'
      this.prepareObservation(result.measurement, result.device.url);
      let bundle = this.packBundle(new PackagingEngine(), result.measurement, device);
      this.log.unshift(JSON.stringify(bundle));
      this.post(bundle);
    }).catch(error => {
      this.log.unshift("error" + JSON.stringify(error));
    });

  }

  public packBundle(
    packagingEngine: PackagingEngine,
    observation: fhir.Observation,
    device: fhir.Device
  ) {

    const observationEntry: BundleEntry = packagingEngine
      .getEntryBuilder()
      .setMethod('POST')
      .setRequestCondition(
        'ifNoneExist',
        'identifier=' + observation.identifier[0].system + '|' + observation.identifier[0].value
      )
      .setResource(observation)
      .build()
    observationEntry.request.url = observation.resourceType
    packagingEngine.addRequestEntry(observationEntry)

    const deviceEntry: BundleEntry = packagingEngine
      .getEntryBuilder()
      .setMethod('POST')
      .setRequestCondition(
        'ifNoneExist',
        'identifier=' + device.identifier[0].system + '|' + device.identifier[0].value
      )
      .setResource(device)
      .build()
    deviceEntry.request.url = device.url
    packagingEngine.addRequestEntry(deviceEntry)

    const patient: fhir.Patient = packagingEngine
      .getPatientBuilder()
      .addIdentifier(
        packagingEngine
          .getIdentifierBuilder()
          .setSystem('urn:oid:1.2.208.176.1.2')
          .setValue('2512489996')
          .build()
      )
      .build()

    const patientRequest: BundleEntry = packagingEngine
      .getEntryBuilder()
      .setMethod('POST')
      .setRequestCondition(
        'ifNoneExist',
        'identifier=' +
        patient.identifier[0].system +
        '|' +
        patient.identifier[0].value
      )
      .setResource(patient).build()
    patientRequest.request.url = patient.resourceType
    patientRequest.fullUrl = 'urn:uuid:d474d2a8-9f94-4836-9094-01108864af54';
    packagingEngine.addRequestEntry(patientRequest)

    return packagingEngine.package()
  }

  private prepareObservation(observation, url) {
    observation.basedOn = [{
      "identifier":
      {
        "system": "urn:ietf:rfc:3986",
        "value": "grøn"
      }
    }];

    observation.contained.forEach(e => {
      (e as any).subject = {
        "reference": "urn:uuid:d474d2a8-9f94-4836-9094-01108864af54",
        "type": "Patient"
      },
        (e as any).device = {
          "reference": url
        }
    });

    observation.subject = {
      "reference": "urn:uuid:d474d2a8-9f94-4836-9094-01108864af54",
      "type": "Patient"
    }
  }

  public post(bundle) {
    fetch('https://gmk.alexandra.dk/fhir_input_service/Bundle/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'SESSION': '2512489996'
      },
      body: JSON.stringify(bundle)
    })
      .then((res) =>
        this.log.unshift("Success")
      )
      .catch(e => { alert("error, " + e) })
  }
}