export interface Callback {
    error(error: String)
    success(message: String)
}