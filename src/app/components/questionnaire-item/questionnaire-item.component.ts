import { Component, Input, Output, EventEmitter, NgZone, OnInit } from "@angular/core";
import { QuestionnaireItem } from "questionnaire-engine";
import { EngineService } from "../../engine.service";
import { DeviceConnector, MonicaDeviceConnector } from "device-measurement-engine";

/**
 * Generated class for the QuestionnaireItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'app-questionnaire-item',
  templateUrl: './questionnaire-item.component.html',
  styleUrls: ['./questionnaire-item.component.scss'],
  inputs: ["item"]
})
export class QuestionnaireItemComponent implements OnInit {
  @Input()
  item: QuestionnaireItem;
  @Output()
  output = new EventEmitter();
  private _answer;

  private deviceConnector: MonicaDeviceConnector;
  private startPressed: boolean = false;

  ngOnInit() {
    
  }


  constructor(public zone: NgZone, public engine: EngineService) {
    this.engine
      .getDeviceMeasurementEngine()
      .getAvailableDevices({
        resourceType: "DeviceDefinition",
        id: "70a46fe8-59d4-4f88-9478-30d14a1466c1",
        type: {
          coding: [
            {
              system: "urn:iso:std:iso:11073:10101",
              code: "8450059",
            }
          ],
          text: ""
        }
      })
      .then(result => {
        this.deviceConnector = result[0] as MonicaDeviceConnector;
      })
      .catch(error => {
        console.log(error);
      });
  }

  ionViewDidLoad() {
    if (this.item.type === "boolean") {
      this.answer = false;
      this.output.emit({ linkId: this.item.linkId, answer: this.answer });
    }
  }

  start() {
    this.startPressed = true;
    this.deviceConnector.startMeasurement().catch(error => {
      throw error;
    });
  }

  stop() {
    this.startPressed = false;
    this.deviceConnector
      .stopMeasurement()
      .then(resultBoolean => {
        if (resultBoolean) {
          return this.deviceConnector.getDeviceAndMeasurement();
        } else {
          throw new Error("failed to start recording");
        }
      })
      .then(result => {
        if (typeof result !== "undefined") {
          this.answer = result;
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  set answer(answer) {
    if (answer.device && answer.observation) {
      let observation = this.engine
        .getDeviceMeasurementEngine()
        .getObservationBuilder()
        .setObservation(answer.observation)
        .setDevice(null, answer.device.identifier[0])
        .build();
      this.engine
        .getPackagingEngine()
        .addRequestEntry(
          this.engine
            .getPackagingEngine()
            .getEntryBuilder()
            .setMethod("POST")
            .setRequestCondition(
              "ifNoneExist",
              "identifier=" +
                observation.identifier[0].system +
                "|" +
                observation.identifier[0].value
            )
            .setResource(answer)
            .build()
        )
        .addRequestEntry(
          this.engine
            .getPackagingEngine()
            .getEntryBuilder()
            .setMethod("POST")
            .setRequestCondition(
              "ifNoneExist",
              "identifier=" +
                answer.device.identifier[0].system +
                "|" +
                answer.device.identifier[0].value
            )
            .setResource(answer.device)
            .build()
        );
      this.zone.run(() => {
        this._answer = answer;
      });
      this.engine
        .getQuestionnaireEngine()
        .setAnswer(this.item.linkId, this.answer);
    } else {
      this.zone.run(() => {
        this._answer = answer;
      });
      this.engine
        .getQuestionnaireEngine()
        .setAnswer(this.item.linkId, this.answer);
    }
  }

  get answer() {
    return this._answer;
  }

  get type() {
    if (this.item.extension) {
      return "observation";
    }
    return this.item.type;
  }

  get label() {
    if (this.answer) {
      return "Ja";
    } else {
      return "Nej";
    }
  }
}
