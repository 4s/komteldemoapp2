// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      'karma-junit-reporter',
      'karma-jasmine',
      'karma-chrome-launcher',
      'karma-summary-reporter',
      '@angular-devkit/build-angular/plugins/karma'
    ],
    summaryReporter: {
      show: 'all'
    },
    junitReporter: {
      outputDir: '../'
    },
    reporters: ['progress', 'junit', 'summary'],
    port: 9876,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadlessNoSandbox'],
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox']
      }
    },
    singleRun: true
  });
};
