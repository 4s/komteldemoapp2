#!/bin/bash

#####
# perform local build of komteldemoapp2 using 
# an image from a repo

repo="docker.alexandra.dk"
imagename="hit-ionic"
version="0.0.1"
cd ..
code_folder=$(pwd)

docker login -u dockerpuller -p OophuePhegohz2ae $repo
docker pull $repo/$imagename:$version

docker run --rm --volume "$code_folder":/data --workdir /data $repo/$imagename:$version rm -rf node_modules plugins platforms www
docker run --rm --volume "$code_folder":/data --workdir /data $repo/$imagename:$version npm ci --no-progress --verbose --no-optional --production 
docker run --rm --volume "$code_folder":/data --workdir /data $repo/$imagename:$version ionic --no-interactive --confirm cordova build android --prod --release
docker run --rm --volume "$code_folder":/data --workdir /data $repo/$imagename:$version jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore /android/debug.keystore platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk androiddebugkey -storepass android

